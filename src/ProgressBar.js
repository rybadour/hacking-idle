import React from 'react';
import './ProgressBar.scss';

function ProgressBar(props) {
  const classes = [
    "ProgressBar",
  ];
  if (props.size === "line") {
    classes.push("line-size");
  }
  const barStyle = {
    width: (props.value * 100) + "%",
  };
  return (
    <div className={classes.join(' ')}>
      <div className="ProgressBar-bar" style={barStyle}></div>
    </div>
  );
}

export default ProgressBar;

