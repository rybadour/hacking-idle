import React, {useState} from 'react';
import './Panel.scss';

function Panel(props) {
  const classes = [
    "Panel",
    props.id + "-panel",
  ];
  if (props.isActive) {
    classes.push("is-active");
  }
  if (props.isUnlocked) {
    classes.push("is-unlocked");
  }
  return (
    <section className={classes.join(' ')}>
      {props.isUnlocked ?
        <h2 onClick={props.onActivate}>{props.name}</h2> :
        <h2>???</h2>
      }

      {props.children}
    </section>
  );
}

export default Panel;
