import React from 'react';
import Panel from './Panel';
import ProgressBar from './ProgressBar';

function IRLPanel(props) {
  return (
    <Panel id="IRL" name="IRL" {...props}>
      <label>Work your day job</label>
      <ProgressBar value={props.workProgress} />
    </Panel>
  );
}

export default IRLPanel;
