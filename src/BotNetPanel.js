import React from 'react';
import Panel from './Panel';

function BotNetPanel(props) {
  return (
    <Panel id="BotNet" name="Bot Net" {...props}>
    </Panel>
  );
}

export default BotNetPanel;
