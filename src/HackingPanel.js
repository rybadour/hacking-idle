import React from 'react';
import './HackingPanel.scss';
import computerImage from './img/CommTerminalWriting.gif';
import ProgressBar from './ProgressBar';
import Panel from './Panel';

function HackingPanel(props) {
  return (
    <Panel id="Hacking" name="Hacking Controls" {...props}>
      <div className="Hacking-computer">
        <img src={computerImage} />
        
        <label>Manual Hacking</label>
        <ProgressBar value={props.manualHackProgress} />
      </div>
    </Panel>
  );
}

export default HackingPanel;
