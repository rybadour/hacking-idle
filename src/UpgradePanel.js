import React from 'react';
import {formatters} from './upgrades';
import './UpgradePanel.scss';
import ProgressBar from './ProgressBar';

function Upgrade(props) {
  const classes = [
    "Upgrade",
  ];
  if (props.affordable) {
    classes.push("affordable");
  }
  return (
    <button type="button" className={classes.join(' ')} onClick={props.unlockAction}>
      <div className="name">{props.name}</div>
      <div className="effect">
        {formatters[props.effect.type](props)}
      </div>
      <div className="cost">
        ${props.cost.toLocaleString()}
      </div>
      <ProgressBar size="line" value={props.completion} />
    </button>
  );
}

function UpgradePanel(props) {
  const upgrades = [];
  for (const up of Object.keys(props.upgrades)) {
    const upgrade = props.upgrades[up];
    if (upgrade.unlocked) continue;
    if (upgrade.reveal) break;
    upgrade.affordable = (upgrade.cost <= props.stats.bitcoin);
    upgrade.completion = (props.stats.bitcoin / upgrade.cost);
    upgrade.unlockAction = props.unlockAction.bind(null, up);

    upgrades.push(<Upgrade {...upgrade} />);
  }

  return (
    <section className="UpgradePanel Panel">
      <h2>Upgrades</h2>

      <div className="UpgradeGrid">
        {upgrades}
      </div>
    </section>
  );
}

export default UpgradePanel;
