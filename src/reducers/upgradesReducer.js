export default (state = {}, action) => {
  switch (action.type) {
    case 'UNLOCK_ACTION':
      const newState = {...state};
      const upgrade = newState[action.upgradeId];
      if (upgrade) {
        upgrade.unlocked = true;
      }
      return newState;

    default:
      return state
  }
}
