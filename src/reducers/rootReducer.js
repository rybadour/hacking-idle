import { combineReducers } from 'redux';
import stats from './statsReducer';
import hacking from './hackingReducer';
import irl from './irlReducer';
import upgrades from './upgradesReducer';

export default combineReducers({
  stats,
  hacking,
  irl,
  upgrades,
});
