import {processEffects} from '../upgrades';

const UPDATE_PER_SEC = 0.25;

export default (state = {}, action) => {
  switch (action.type) {
    case 'WORKING_ACTION':
      const amount = (action.timestep/1000 * UPDATE_PER_SEC);
      const progress = state.workProgress + processEffects(amount, "foobar", action.upgrades);
      return {
        workProgress: progress, 
      };

    case 'WORK_COMPLETE_ACTION':
      return {
        manualHackProgress: 0,
      };

    default:
      return state
  }
}
