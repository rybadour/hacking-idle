import upgrades from '../upgrades';
const BITCOIN_PER_HACK = 1;

export default (state = {}, action) => {
  switch (action.type) {
    case 'HACK_COMPLETE_ACTION':
      return {
        ...state,
        bitcoin: state.bitcoin + BITCOIN_PER_HACK,
      };

    case 'UNLOCK_ACTION':
      let bitcoin = state.bitcoin;
      const upgrade = upgrades[action.upgradeId];
      if (upgrade) {
        bitcoin -= upgrade.cost;
      }
      return {
        ...state,
        bitcoin,
      };

    default:
      return state;
  }
}
