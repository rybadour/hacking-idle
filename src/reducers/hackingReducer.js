import {processEffects, HACK_SPEED} from '../upgrades';

const UPDATE_PER_SEC = 0.25;

export default (state = {}, action) => {
  switch (action.type) {
    case 'HACKING_ACTION':
      const amount = (action.timestep/1000 * UPDATE_PER_SEC);
      const progress = state.manualHackProgress + processEffects(amount, HACK_SPEED, action.upgrades);
      return {
        manualHackProgress: progress, 
      };

    case 'HACK_COMPLETE_ACTION':
      return {
        manualHackProgress: 0,
      };

    default:
      return state
  }
}
