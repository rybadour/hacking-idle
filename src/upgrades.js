const PRIMARY_CURRENCY = "bitcoin";
const IRL_FEATURE = "irl";
const BOT_NET_FEATURE = "bot-net";
export const HACK_SPEED = "hacking-speed";

export const formatters = {
  "unlock": function (upgrade) {
    let name = "???";
    switch (upgrade.effect.value) {
      case IRL_FEATURE:
        name = "IRL";
        break;
      case BOT_NET_FEATURE:
        name = "Bot Net";
        break;
    }

    return "Unlock " + name;
  },
  [HACK_SPEED]: function (upgrade) {
    return "Hacking speed x" + upgrade.effect.value;
  },
};

export function processEffects(baseValue, type, upgrades) {
  let value = baseValue;
  Object.values(upgrades)
    .filter((up) => (up.unlocked && up.effect.type === type))
    .map((up) => up.effect.value)
    .forEach((effectValue) => {
      value *= effectValue;
    });
  return value;
}

export default {
  // hidden upgrade used to enable hacking by default
  unlock_hacking: {
    unlocked: true,
    name: "N/A",
    cost: 0,
    effect: {type: "unlock", value: IRL_FEATURE},
  },
  speedup_hack1: {
    name: "Mechanical Keyboard",
    cost: 5,
    effect: {type: HACK_SPEED, value: 1.5},
  },
  speedup_hack2: {
    name: "Dual Monitors",
    cost: 10,
    effect: {type: HACK_SPEED, value: 2},
  },
  unlock_irl: {
    name: "Leave the house",
    cost: 20,
    effect: {type: "unlock", value: IRL_FEATURE},
  },
  unlock_botnet: {
    name: "Publish Game Of Thrones final episode with virus",
    cost: 100,
    effect: {type: "unlock", value: BOT_NET_FEATURE},
    reveal: {currency: PRIMARY_CURRENCY, value: 50},
  },
};
