import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';
import upgrades from './upgrades';

const initialState = {
  stats: {
    bitcoin: 0,
    money: 5,
    infectedComputers: 0,
  },
  hacking: {
    manualHackProgress: 0,
  },
  irl: {
    workProgress: 0,
  },
  upgrades: upgrades,
};

export default function configureStore(state=initialState) {
  return createStore(
    rootReducer,
    state,
    applyMiddleware(thunk)
  );
}
