export const workingAction = (timestep, upgrades) => dispatch => {
  dispatch({
    type: 'WORKING_ACTION',
    timestep,
    upgrades,
  })
}

export const workCompleteAction = () => dispatch => {
  dispatch({
    type: 'WORK_COMPLETE_ACTION',
  })
}
