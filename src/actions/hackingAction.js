export const hackingAction = (timestep, upgrades) => dispatch => {
  dispatch({
    type: 'HACKING_ACTION',
    timestep,
    upgrades,
  })
}

export const hackCompleteAction = () => dispatch => {
  dispatch({
    type: 'HACK_COMPLETE_ACTION',
  })
}
