import React, {useState} from 'react';
import { connect } from 'react-redux';
import {hackingAction, hackCompleteAction} from './actions/hackingAction';
import {unlockAction} from './actions/upgrades';
import './App.scss';

import BotNetPanel from './BotNetPanel.js';
import HackingPanel from './HackingPanel.js';
import IRLPanel from './IRLPanel.js';
import UpgradePanel from './UpgradePanel.js';

const HACKING_PANEL = 'hacking';
const BOT_NET_PANEL = 'botnet';
const IRL_PANEL = 'irl';

let loopCreated = false;

function App(props) {
  const [currentPanel, setPanel] = useState(HACKING_PANEL);

  if (!loopCreated) {
    loopCreated = true;
    loop((timestep) => {
      loopCreated = false;
      if (currentPanel === HACKING_PANEL) {
        if (props.hacking.manualHackProgress >= 1) {
          props.hackCompleteAction();
        } else {
          props.hackingAction(timestep, props.upgrades);
        }
      } else if (currentPanel === IRL_PANEL) {
        if (props.irl.workProgress >= 1) {
          props.workCompleteAction();
        } else {
          props.workingAction(timestep, props.upgrades);
        }
      }
    });
  }

  function mixinActivation(panelProps, panel) {
    panelProps.isUnlocked = props.upgrades["unlock_" + panel].unlocked;
    panelProps.isActive = currentPanel === panel;
    panelProps.onActivate = setPanel.bind(this, panel);
  }

  const hackingProps = {
    ...props.hacking,
  };
  mixinActivation(hackingProps, HACKING_PANEL);

  const botNetProps = {
  };
  mixinActivation(botNetProps, BOT_NET_PANEL);

  const irlProps = {
    ...props.irl,
  };
  mixinActivation(irlProps, IRL_PANEL);

  const upgradeProps = {
    upgrades: props.upgrades,
    stats: {
      bitcoin: props.stats.bitcoin,
    },
    unlockAction: props.unlockAction,
  };

  return (
    <div className="App">
      <header className="App-header">
        Hacking Idle!
      </header>

      <div className="Stats">
        <div>
          <label>Bitcoin</label>
          <span>₿{props.stats.bitcoin.toLocaleString()}</span>
        </div>
        <div>
          <label>Cash</label>
          <span>${props.stats.money.toLocaleString()}</span>
        </div>
        <div>
          <label>Infected Computers</label>
          <span>{props.stats.infectedComputers.toLocaleString()}</span>
        </div>
      </div>
        

      <div className="Panels">
        <BotNetPanel {...botNetProps} />
        <HackingPanel {...hackingProps} />
        <IRLPanel {...irlProps} />
      </div>

      <UpgradePanel {...upgradeProps} />
    </div>
  );
}

const mapStateToProps = state => ({
 ...state
});

const mapDispatchToProps = dispatch => ({
 hackingAction:      (timestep, upgrades) => dispatch(hackingAction(timestep, upgrades)),
 hackCompleteAction: () => dispatch(hackCompleteAction()),
 workingAction:      (timestep, upgrades) => dispatch(hackingAction(timestep, upgrades)),
 workCompleteAction: () => dispatch(hackCompleteAction()),
 unlockAction:       (upgradeId) => dispatch(unlockAction(upgradeId))
});

function loop(func) {
  const before = Date.now();
  window.requestAnimationFrame(() => {
    const timestep = Math.round(Date.now() - before);
    func(timestep);
  });
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
